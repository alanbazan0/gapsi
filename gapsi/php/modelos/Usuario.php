<?php
namespace php\modelos;

class Usuario
{
    public $id;
    public $nombreUsuario;
    public $contrasena;
    public $nombre;
    public $apellido;
    public $empresaId;
    public $sedeId;
    public $puestoId;
    public $areaId;
    public $supervisor1Id;
    public $supervisor2Id;
    public $supervisor3Id;
    public $tipoUsuarioId;
    public $estatus;
    public $nombreCompleto;
    public $fotoPerfil;
}

