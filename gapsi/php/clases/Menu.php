<?php
namespace php\clases;

use TipoUsuario;

include "php/clases/TipoUsuario.php";


class Menu 
{
    private $API;
    private function crearOpcion($titulo,$url,$icono)
    {
        $opcion="<li><a href='#' onClick='menuVista.abrirUrl(\"".$url."\");'><i class='fa ".$icono." fa-lg' ></i><span class='nav-text'>".$titulo."</span></a></li>";
        return $opcion;
    }
    
    private function renderizarOpcionSecundaria($titulo,$url)
    {
        $opcion="<li><a href='$url'>$titulo</a></li>";
        return $opcion;
    }

    
    private function renderizarOpcion($titulo,$url,$icono)
    {
        $opcion=" <li><a href='$url'><i class='$icono' style='width:25px;'></i> <span>$titulo</span></a></li>";
        return $opcion;
    }
    
    private function renderizarOpcionMensajes()
    {
        $opcion=" <li>
                <a id='mensajesLi' href='mensajes.php'><i class='fa fa-envelope' style='width:25px;'></i> <span>Mensajes</span>
                <span  id='mensajesNotificacionSpan' class='pull-right-container'>
               
                </span>
                </a>
                </li>";
        return $opcion;
    }
    

    public function setAPI($API)
    {
        $this->API = $API;
    }
    
    public function getAppId()
    {
        return "SAHA";
    }
    
    
    
    private function renderizarOpcionSolicitudRevisionProcesos(){
       return $this->renderizarOpcion("Revisión de procesos","solicitud_revision_procesos.php","fas fa-check");
    }
    
    private function renderizarOpcionReportes()
    {
        $opciones="<li class='treeview'>";
        $opciones.="<a href='#'>";
        $opciones.="<i class='fa fa-file'></i> <span> Reportes</span>";
        $opciones.="<span class='pull-right-container'>";
        $opciones.="<i class='fa fa-angle-left pull-right'></i>";
        $opciones.="</span>";
        $opciones.="</a>";
        $opciones.="<ul class='treeview-menu'>";
        $opciones.=$this->renderizarOpcion("Reporte 1","reporte1.php","fas fa-file-pdf");
        $opciones.=$this->renderizarOpcion("Reporte 2","reporte2.php","fas fa-table");
        $opciones.="</ul>";
        $opciones.="</li>";
        $opciones.="<li>";
        return $opciones;
    }
    
    
    private function renderizarCatalogosAdministrador($estiloLista)
    {
        $opciones="";
        $opciones.="<li class='treeview'>";
        $opciones.="<a href='#'>";
        $opciones.="<i class='fa fa-table'></i> <span>Catálogos</span>";
        $opciones.="<span class='pull-right-container'>";
        $opciones.="<i class='fa fa-angle-left pull-right'></i>";
        $opciones.="</span>";
        $opciones.="</a>";
        $opciones.="<ul class='treeview-menu'>";
        $opciones.=$this->renderizarOpcion("Proveedores","proveedores.php","fa fa-circle-o");
        
        $opciones.="</ul>";
        $opciones.="</li>";
        $opciones.="<li>";
        //TODO: Agregar mas opciones segun el tipo de usuario ADMINISTRADOR
//         $opciones.=$this->renderizarOpcionReportes();
        
//         $opciones.="<li class='treeview'>";
//         $opciones.="<a href='#'>";
//         $opciones.="<i class='fa fa-chart-bar'></i> <span>Gráficas</span>";
//         $opciones.="<span class='pull-right-container'>";
//         $opciones.="<i class='fa fa-angle-left pull-right'></i>";
//         $opciones.="</span>";
//         $opciones.="</a>";
//         $opciones.="<ul class='treeview-menu'>";
//         $opciones.=$this->renderizarOpcion("Grafica 1","grafica1.php","fa fa-circle-o");
//         $opciones.=$this->renderizarOpcion("Grafica 2","grafica2.php","fa fa-circle-o");
//         $opciones.=$this->renderizarOpcion("Grafica 3","grafica3.php","fa fa-circle-o");
//         $opciones.=$this->renderizarOpcion("Grafica 4","grafica4.php","fa fa-circle-o");
//         $opciones.="</ul>";
//         $opciones.="</li>";
//         $opciones.="<li>";
        
//         $opciones.=$this->renderizarOpcion("Validación evidencias","evidencias.php","fas fa-check-square");
        
//         $opciones.=$this->renderizarOpcion("Opcion 1","opcion1.php","fas fa-tasks");
//         $opciones.=$this->renderizarOpcion("Opcion 2","opcion2.php","fas fa-check");
        
        
        return $opciones;
    }
    
    
    public function renderizarIndicadoresSupervisor()
    {
        $indicadores="";
//         $indicadores.=$this->renderizarIndicador("Areas", "areas", "areas.php", "#7842fa", "fas fa-cubes");
//         $indicadores.=$this->renderizarIndicador("Puestos", "puestos", "puestos.php", "#fa4251", "fas fa-male");
//         $indicadores.=$this->renderizarIndicador("Usuarios", "usuarios", "usuarios.php", " #ff8300", "fa fa-users");
        return $indicadores;
    }
    
    public function renderizarIndicadoresUsuario()
    {
        $indicadores="";
//         $indicadores.=$this->renderizarIndicador("Areas", "areas", "areas.php", "#7842fa", "fas fa-cubes");
//         $indicadores.=$this->renderizarIndicador("Puestos", "puestos", "puestos.php", "#fa4251", "fas fa-male");
//         $indicadores.=$this->renderizarIndicador("Usuarios", "usuarios", "usuarios.php", " #ff8300", "fa fa-users");
        return $indicadores;
    }
    
    public function renderizarIndicadoresCoordinador()
    {
        $indicadores="";
//         $indicadores.=$this->renderizarIndicador("Sedes", "sedes", "sedes.php", "#00b5e9", "fas fa-list");
//         $indicadores.=$this->renderizarIndicador("Areas", "areas", "areas.php", "#7842fa", "fas fa-cubes");
//         $indicadores.=$this->renderizarIndicador("Puestos", "puestos", "puestos.php", "#fa4251", "fas fa-male");
//         $indicadores.=$this->renderizarIndicador("Usuarios", "usuarios", "usuarios.php", " #ff8300", "fa fa-users");
        return $indicadores;
    }
    
    
    public function renderizarFooter()
    {
        $html="";
        $html.="<footer class='main-footer'>";
        $html.="<div class='pull-right hidden-xs'>";
        $html.="</div>";
        $html.="<strong>Copyright &copy; 2020 <a href=''></a>.</strong> Todos los derechos reservados.";
        $html.="</footer>";
        echo $html;
    }
    
    
    public function renderizar($tipoUsuarioId)
    {
        $menu = "<li class='header'>Menú</li>";
        //$menu.=$this->renderizarOpcion("Panel de control", "panel.php", "fas fa-tachometer-alt");
        if($tipoUsuarioId== TipoUsuario::ADMINISTRADOR)
            $menu.= $this->renderizarCatalogosAdministrador("");

        echo $menu;
    }
    
    public function renderizarFotoPerfilIzquierda($usuario)
    {
        $fecha = new \DateTime();
        $time = $fecha->getTimestamp();
        $html="";
        $html.="<div class='pull-left image'>";
        $html.="<img  id='imgFotoPefil2' src='$this->API/$usuario->fotoPerfil?$time' class='img-circle' alt='User Image'>";
        $html.="</div>";
        $html.="<div class='pull-left info'>";
        $html.="<p>$usuario->nombreCompleto</p>";
        $html.="<a href='#'><i class='fa fa-circle text-success'></i> En linea</a>";
        $html.="</div>";
        echo $html;
    }
    
    public function renderizarFotoPerfilSuperior($usuario)
    {
        
       
        
        $fecha = new \DateTime();
        $time = $fecha->getTimestamp();
        $html="";
        
        $html.=$this->renderizarBotones($usuario);
        
        $html.="<!-- User Account: style can be found in dropdown.less -->";
        $html.="<li class='dropdown user user-menu'>";
        $html.="<a href='#' class='dropdown-toggle' data-toggle='dropdown'>";
        $html.="<img id='imgFotoPefil3' src='$this->API/$usuario->fotoPerfil?$time' class='user-image' alt='User Image'>";
        $html.="<span class='hidden-xs'>$usuario->nombreCompleto</span>";
        $html.="</a>";
        $html.="<ul class='dropdown-menu'>";
        $html.="<!-- User image -->";
        $html.="<li class='user-header'>";
        $html.="<img id='imgFotoPefil1' src='$this->API/$usuario->fotoPerfil?$time' class='img-circle' alt='User Image'>";
        $html.="<input type='file' id='perfilFile'  name='file' style='display:none' onchange='vista.subirFotoPerfil(this);'>";
        $html.="<p>";
        $html.= $usuario->nombreCompleto ." - ". $usuario->tipoUsuarioNombre;
//         $html.="<small>Member since Nov. 2012</small>";
        $html.="</p>";
        $html.="</li>";
        $html.="<!-- Menu Body -->";
//         $html.="<li class='user-body'>";
//         $html.="<div class='row'>";
//         $html.="<div class='col-xs-4 text-center'>";
//         $html.="<a href='#'>Followers</a>";
//         $html.="</div>";
//         $html.="<div class='col-xs-4 text-center'>";
//         $html.="<a href='#'>Sales</a>";
//         $html.="</div>";
//         $html.="<div class='col-xs-4 text-center'>";
//         $html.="<a href='#'>Friends</a>";
//         $html.="</div>";
//         $html.="</div>";
//         $html.="<!-- /.row -->";
//         $html.="</li>";
        $html.="<!-- Menu Footer-->";
        $html.="<li class='user-footer'>";
        $html.="<div class='pull-left'>";
        $html.="<a href='#' class='btn btn-default btn-flat' onclick='vista.cambiarFotoPerfil();'>Foto de perfil</a>";
        $html.="</div>";
        $html.="<div class='pull-right'>";
        $html.="<a href='#' class='btn btn-default btn-flat' onclick='vista.cerrarSesion();'>Cerrar sesión</a>";
        $html.="</div>";
        $html.="</li>";
        $html.="</ul>";
        $html.="</li>";
        echo $html;
    }
    
    public function renderizarUsuarioJSON($usuario)
    {
        $json = "";
        if(isset($usuario))
        {
            $usuario->contrasena ="";        
           // $json = json_encode($usuario, JSON_UNESCAPED_UNICODE);
            $json = json_encode($usuario, JSON_PRETTY_PRINT);
        }
        echo "data-usuario='$json'";
    }
    
    public function renderizarBotones($usuario)
    {
        $html="";
       
        echo $html;
    }
    
    public function renderizarFotoPerfil($usuario)
    {
        $fecha = new \DateTime();
        $time = $fecha->getTimestamp();
        $html="";
        $html.="<div id='menuPerfil' class='account-item clearfix js-item-menu'>";
        $html.="    <div class='image'>";
        $html.="        <img id='imgFotoPefil1' src='$usuario->fotoPerfil?$time' alt='$usuario->nombreCompleto' />";
        $html.="    </div>";
        $html.="    <div class='content'>";
        $html.="        <a class='js-acc-btn' href='#'>$usuario->nombreCompleto</a>";
        $html.="    </div>";
        $html.="    <div class='account-dropdown js-dropdown'>";
        $html.="        <div class='info clearfix'><div class='info clearfix'>";
        $html.="            <div class='image'>";
        $html.="                <a href='#'>";
        $html.="                    <img id='imgFotoPefil2' src='$usuario->fotoPerfil' alt='$usuario->nombreCompleto' />";
        $html.="                </a>";
        $html.="            </div>";
        $html.="            <div class='content'>";
        $html.="                <h5 class='name'>";
        $html.="                    <a href='#'>$usuario->nombreCompleto</a>";
        $html.="                </h5>";
        $html.="                <span class='email'>$usuario->nombreUsuario</span>";
        $html.="            </div>";
        $html.="        </div>";
        $html.="        <div class='account-dropdown__body'>";
        $html.="            <div class='account-dropdown__item'>";
        $html.="                <a href='#' onclick='vista.cambiarFotoPerfil();'>";
        $html.="                <i class='zmdi zmdi-face'></i>Foto de perfil</a>";
        $html.="                 <input type='file' id='perfilFile'  name='file' style='display:none' onchange='vista.subirFotoPerfil(this);'";
        $html.="            </div>";
        $html.="        </div>";
        $html.="        <div class='account-dropdown__footer'>";
        $html.="            <a href='#' onclick='vista.cerrarSesion();'>";
        $html.="            <i class='zmdi zmdi-power'></i>Cerrar sesión</a>";
        $html.="        </div>";
        $html.="    </div>";
        $html.="</div>";
        //$html= file_get_contents('php/plantillas_html/foto_perfil.html');
        echo $html;
    }
    
    public function getUsuario($usuario)
    {
        $usuarioJavascript= (object) [
            'tipoUsuarioId' => $usuario->tipoUsuarioId
        ];
        return $usuarioJavascript;
    }
    
}