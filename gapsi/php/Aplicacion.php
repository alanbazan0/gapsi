<?php

class Aplicacion
{
    
    
    public function iniciarTodos()
    {
        return $this->iniciar(array(TipoUsuario::ADMINISTRADOR,
            TipoUsuario::COORDINADOR,
            TipoUsuario::SUPERVISOR,
            TipoUsuario::USUARIO,
            TipoUsuario::INSPECTOR,
            TipoUsuario::CAPACITADO));
    }
    
    public function iniciar($tiposUsuario)
    {
        session_start();
        $usuario = null;
        $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        if(isset($_SESSION['usuario']))
        {
            $usuario = $_SESSION['usuario'];
            if(!$this->tienePermiso($usuario,$tiposUsuario))
            {
                $inicioSesion = "inicio_sesion.php";
                if($url!=null && $url!="")
                    $inicioSesion.="?url=".$url;
                    header('Location: '.$inicioSesion);
            }
            else
            {
            }
        }
        else
        {
            $inicioSesion = "inicio_sesion.php";
            if($url!=null && $url!="")
                $inicioSesion.="?url=".$url;
                header('Location: '.$inicioSesion);
        }
        return $usuario;
    }
    

    
    function tienePermiso($usuario,$tiposUsuario)
    {
        for ($i = 0; $i < count($tiposUsuario); $i++) 
        {
            if($usuario->tipoUsuarioId == $tiposUsuario[$i] && $usuario->estatus==1)
                return true;
        }
      
        return false;
    }
    
    public function getUsuario($usuario)
    {
        $datosUsuario= (object) [
            'id' => $usuario->id,
            'empresaId' => $usuario->empresaId,
            'sedeId' => $usuario->sedeId
        ];
        return $usuario;
    }
    
 
    
 
    
}