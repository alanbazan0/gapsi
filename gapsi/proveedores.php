<?php
use php\clases\Menu;

include 'php/configuracion.php';
include 'php/Aplicacion.php';
include 'php/clases/Menu.php';

$aplicacion = new Aplicacion();
$usuario = $aplicacion->iniciar(array(TipoUsuario::ADMINISTRADOR));

$menu = new Menu();
$menu->setAPI(getAPI());
$titulo = "Proveedores";
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Last-Modified" content="0">
	<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
	<meta http-equiv="Pragma" content="no-cache">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon"  href="img/favicon.ico"/>   
  <title>SAHA - <?php echo $titulo;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
<!--   <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.min.css"> -->
<!--   <link rel="stylesheet" href="lib/font-awesome-5/css/fontawesome-all.min.css"> -->
  <!-- Ionicons -->
  <link rel="stylesheet" href="lib/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="lib/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="lib/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="lib/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="lib/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="lib/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
 <!--  Librerias comunes -->
  <link href="<?php echo getAPI()?>/lib/DataTables/datatables.min.css" rel="stylesheet" media="all">	
  <link href="<?php echo getAPI()?>/lib/toastr/toastr.min.css" rel="stylesheet">
  <link href="<?php echo getAPI()?>/lib/sweetalert/sweetalert.css" rel="stylesheet">
  <link href="<?php echo getAPI()?>/lib/switch/switch.css" rel="stylesheet">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  
    <!-- style -->
  <link rel="stylesheet" href="css/style.css">
	
  
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="panel.php" class="logo" >
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="img/logo_hamburger.png"></img></span>
      <!-- logo for regular state and mobile devices -->
       <span ><img src="img/logo_hamburger.png"></img></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

       		<?php $menu->renderizarFotoPerfilSuperior($usuario);?>
         
          <!-- Control Sidebar Toggle Button -->
<!--           <li> -->
<!--             <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a> -->
<!--           </li> -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
         <?php $menu->renderizarFotoPerfilIzquierda($usuario);?>

      </div>

      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
          <?php 
              $menu->renderizar($usuario->tipoUsuarioId);
          ?> 
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $titulo;?>
      </h1>
      <ol class="breadcrumb">
<!--         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> -->
<!--         <li class="active">Dashboard</li> -->
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <!-- 	Filtros     -->
    	 <div class="row">
            <div class="col-md-12">
               <!-- DATA TABLE -->
						<div class="table-data__tool">
							<div class="table-data__tool-left">
								<div class="rs-select2--light rs-select2--md ">
									<label for="nombreInputCriterio" class="control-label mb-1">Nombre</label>
									<input id="nombreInputCriterio" name="nombreInputCriterio"
										type="text" class="form-control" aria-required="true"
										aria-invalid="false">
								</div>
								<div class="rs-select2--light rs-select2--md ">
									<label for="razonSocialInputCriterio" class="control-label mb-1">Razón Social</label>
									<input id="razonSocialInputCriterio" name="razonSocialInputCriterio"
										type="text" class="form-control" aria-required="true"
										aria-invalid="false">
								</div>
								<div class="rs-select2--light rs-select2--md ">
									<label for="direccionInputCriterio" class="control-label mb-1">Dirección</label>
									<input id="direccionInputCriterio" name="direccionInputCriterio"
										type="text" class="form-control" aria-required="true"
										aria-invalid="false">
								</div>
								 <button id="consultarButton" class="btn btn-info" >
                       			 <i class="fa fa-filter"></i>Filtrar</button>
							</div>
							 <div class="table-data__tool-right">
                                <button id="agregarButton" class="btn btn-success">
                                 <i class="fa fa-plus"></i>Agregar</button>
                            </div>
						</div>
						
						<!-- END DATA TABLE -->
					</div>
        </div>
 		
      <div class="row">
       	<div class='col-xs-12'>
        	<div class="box">
                <div class="box-body">
                 	<div id="tabla"></div>                		
                </div>
              </div>
    		</div>
	   </div>
    </section>
    <!-- /.content -->
  </div>
  
  
  <!-- /.content-wrapper -->
   <?php $menu->renderizarFooter($usuario->tipoUsuarioId);?>
  

  
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="lib/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="lib/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<!-- Morris.js charts -->
<script src="lib/raphael/raphael.min.js"></script>
<script src="lib/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="lib/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="lib/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="lib/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="lib/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="lib/moment/min/moment.min.js"></script>
<script src="lib/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="lib/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="lib/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="lib/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="lib/fastclick/lib/fastclick.js"></script>

<!-- Librerias comunes -->
<script src="<?php echo getAPI()?>/lib/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo getAPI()?>/lib/toastr/toastr.min.js"></script>
<script src="<?php echo getAPI()?>/lib/form-validation/jquery.validate.min.js"></script>
<script src="<?php echo getAPI()?>/lib/DataTables/datatables.min.js"></script>  



<!-- Clases base -->
<script src="<?php echo getAPI()?>/js/clases/configuracion.js"></script>
<script src="<?php echo getAPI()?>/js/clases/modo.js"></script>
<script src="<?php echo getAPI()?>/js/componentes/tabla.js"></script>  
<script src="<?php echo getAPI()?>/js/presentadores/catalogo_presentador.js"></script>
<script src="<?php echo getAPI()?>/js/vistas/vista.js"></script>
<script src="<?php echo getAPI()?>/js/vistas/catalogo_vista.js"></script>
<script src="<?php echo getAPI()?>/js/repositorios/repositorio.js"></script>
<script src="<?php echo getAPI()?>/js/clases/tipo_usuario.js"></script>  
<!-- Repositorios -->
<script src="<?php echo getAPI()?>/js/repositorios/usuarios_repositorio.js"></script>
<script src="<?php echo getAPI()?>/js/repositorios/proveedores_repositorio.js"></script>
<!-- Presentador y Vista -->
<script src="<?php echo getAPI()?>/js/presentadores/proveedores_presentador.js"></script>
<script src="<?php echo getAPI()?>/js/vistas/proveedores_vista.js"></script>

<!-- Main JS-->
<script src="<?php echo getAPI()?>/js/main.js"></script>

<!-- AdminLTE App -->
<script src="js/adminlte.min.js"></script>

  <img  src='img/cargando.svg' id="indicador" style="z-index:99999;display:none"></img> 
</body>
</html>
