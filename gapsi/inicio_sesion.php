<?php 
include 'php/configuracion.php';

session_start();

$url="";
if(isset($_GET['url']))
    $url = $_GET['url'];

 $url =   str_replace("$","&",$url);

?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Last-Modified" content="0">
	<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
	<meta http-equiv="Pragma" content="no-cache">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GAPSI</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   <link rel="shortcut icon"  href="img/favicon.ico"/>   
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="lib/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="lib/iCheck/square/blue.css">
  <!--  Toastr -->
  <link href="<?php echo getAPI()?>/lib/toastr/toastr.min.css" rel="stylesheet">
  <!--  SweeAlert -->
  <link href="<?php echo getAPI()?>/lib/sweetalert/sweetalert.css" rel="stylesheet">
   <!-- style -->
  <link rel="stylesheet" href="css/style.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page" data-aplicacionId="SAHA" data-aplicacionVersion="html" data-url="<?php echo $url; ?>" >
<div class="login-box">
  
  <!-- /.login-logo -->
  <div class="login-box-body">
  <div class="login-logo">
    <img id="profile-img" src="img/login_logo.png" class='img-responsive center-block'/>
  </div>
    <p class="login-box-msg">Inicio de sesión</p>

    <form id="inicioSesionForm" action="#" method="post">
      <div class="form-group has-feedback">
      	<div>
            <input id="nombreUsuarioInput" name="nombreUsuarioInput" type="email" class="form-control" placeholder="Correo electrónico">
            <span  class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
      </div>
      <div class="form-group has-feedback">
      	<div>
            <input id="contrasenaInput" name="contrasenaInput" type="password" class="form-control" placeholder="Contraseña">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
         </div>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
<!--               <input type="checkbox"> Remember Me -->
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button id="ingresarButton" type="submit" class="btn btn-primary btn-block btn-flat">Ingresar</button>
        </div>
        
        
        <!-- /.col -->
      </div>
      <span id="versionSpan"></span>
    </form>

    <div class="social-auth-links text-center">
<!--       <p>- OR -</p> -->
<!--       <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using -->
<!--         Facebook</a> -->
<!--       <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using -->
<!--         Google+</a> -->
    </div>
    <!-- /.social-auth-links -->


  </div>
  <!-- /.login-box-body -->
</div>

<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="lib/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<!-- iCheck -->
<script src="lib/iCheck/icheck.min.js"></script>



<!-- Librerias comunes -->
<script src="<?php echo getAPI()?>/lib/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo getAPI()?>/lib/toastr/toastr.min.js"></script>
<script src="<?php echo getAPI()?>/lib/form-validation/jquery.validate.min.js"></script>

<script src="<?php echo getAPI()?>/js/clases/configuracion.js"></script>
<script src="<?php echo getAPI()?>/js/repositorios/repositorio.js"></script>
<script src="<?php echo getAPI()?>/js/vistas/vista.js"></script>
<script src="<?php echo getAPI()?>/js/repositorios/usuarios_repositorio.js"></script>
<script src="<?php echo getAPI()?>/js/repositorios/aplicacion_repositorio.js"></script>
<script src="<?php echo getAPI()?>/js/presentadores/inicio_sesion_presentador.js"></script>
<script src="<?php echo getAPI()?>/js/vistas/inicio_sesion_vista.js"></script>

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
  <img  src='img/cargando.svg' id="indicador" style="display:none"></img> 
</body>
</html>
